/**
 *
 * 
 * Author: Apisit Aphiphatthanaphokhin
 * ID: 613040105-9
 * Sec: 1
 * Date: April 2, 2019
 *
 */
package aphiphatthanaphokhin.apisit.lab8;

import javax.swing.SwingUtilities;

public class MyFrameV3 extends MyFrameV2{

	public MyFrameV3(String text) {
		super(text);
		
	}
	
	public static void main(String[] args) {
		SwingUtilities.invokeLater(new Runnable() {
			public void run() {
				createAndShowGUI();
			}
		});
	}
	
	public static void createAndShowGUI() {
		MyFrameV3 msw = new MyFrameV3("My Frame V3");
		msw.addComponents();
		msw.setFrameFeatures();
	}
	
	protected void addComponents() {
		add(new MyCanvasV3());
	}
	
}
