/**
 *
 * 
 * Author: Apisit Aphiphatthanaphokhin
 * ID: 613040105-9
 * Sec: 1
 * Date: April 2, 2019
 *
 */
package aphiphatthanaphokhin.apisit.lab8;

import java.awt.Color;
import java.awt.Component;
import java.awt.Dimension;
import java.awt.Graphics;

import javax.swing.JPanel;

public class MyCanvas extends JPanel {
	
	public static int WIDTH = 800;
	public static int HEIGHT = 600;
	
	public MyCanvas() {
		super();
		setPreferredSize(new Dimension(WIDTH,HEIGHT));
		setBackground(Color.black);
	}
	
	public void paint(Graphics g) {
		
		super.paint(g);
		int widht = this.getWidth();
		int height = this.getHeight();
		int w = widht / 2;
		int h = height / 2;
		g.setColor(Color.WHITE);
		g.drawOval(w - 150 ,h - 150, 300, 300);
		g.fillOval(w - 50, h - 50, 30, 60);
		g.fillOval(w + 20, h - 50, 30, 60);
		g.fillRect(w - 50, h + 75, 100, 10);
	}

	public void paint() {
		// TODO Auto-generated method stub
		
	}

	
}
