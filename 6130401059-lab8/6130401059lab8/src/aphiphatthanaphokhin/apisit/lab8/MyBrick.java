/**
 *
 * 
 * Author: Apisit Aphiphatthanaphokhin
 * ID: 613040105-9
 * Sec: 1
 * Date: April 2, 2019
 *
 */
package aphiphatthanaphokhin.apisit.lab8;

import java.awt.Color;
import java.awt.Dimension;
import java.awt.geom.Rectangle2D;

public class MyBrick extends Rectangle2D.Double{
	
	public static int brickWidth = 80 , brickHeight = 20;
	public MyBrick(int x,int y) {
		super(x,y,brickWidth,brickHeight);
	}

}
