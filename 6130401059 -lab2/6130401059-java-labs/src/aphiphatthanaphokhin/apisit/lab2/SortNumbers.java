/**
* This Java program that that accept five decimals arguments to sort these numbers
*
* Author: Apisit Aphiphatthanaphokhin
* ID: 613040105-9
* Sec: 1
* Date: January 25, 2019
*
**/
package aphiphatthanaphokhin.apisit.lab2;

import java.util.Arrays;
public class SortNumbers {

	    public static void main(String[] args) 
	    { 
	        
	        float[] arynumb;
	        arynumb = new float[5];
	        
	        arynumb[0] = (float) Double.parseDouble(args[0]);
	        arynumb[1] = (float) Double.parseDouble(args[1]);
	        arynumb[2] = (float) Double.parseDouble(args[2]);
	        arynumb[3] = (float) Double.parseDouble(args[3]);
	        arynumb[4] = (float) Double.parseDouble(args[4]);
	  
	        Arrays.sort(arynumb);
	  
	        System.out.printf(Arrays.toString(arynumb).replace("[", "").replace("]", "")); 
	    } 
	} 