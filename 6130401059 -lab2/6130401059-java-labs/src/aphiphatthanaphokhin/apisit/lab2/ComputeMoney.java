/**
* Compute how much money Wanlee has. She will input the number of 
* notes of 1,000 Baht, 500 Baht, 100 Baht and 20 Baht, 
* the program should output the total amount of money she has.
* If Wanlee does not enter all four numbers for bank notes, 
* the error message should be output as shown in Figure 7.
* 
* Author: Apisit Aphiphatthanaphokhin
* ID: 613040105-9
* Sec: 1
* Date: January 25, 2019
*
**/

package aphiphatthanaphokhin.apisit.lab2;

public class ComputeMoney {
	public static void main(String[] args) {
		if( args.length == 4) {
			
		double a  = Double.parseDouble(args[0]);
		double b  = Double.parseDouble(args[1]);
		double c  = Double.parseDouble(args[2]);
		double d  = Double.parseDouble(args[3]);
		double sum = (1000*a + 500*b + 100*c + 20*d);
		
			System.out.println("Total Money is " + sum);
		}
			else if( args.length != 4) {
				System.out.println("Error message, output when the number of argument is not four. ");
			}
	}
}
