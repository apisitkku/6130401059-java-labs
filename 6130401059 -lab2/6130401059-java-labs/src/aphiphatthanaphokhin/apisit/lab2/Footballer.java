/**
 * This Java program that accepts three arguments: your favorite football player, the
*  football club that the player plays for, and the nationality of that player. The output of
*  the program is in the format
* "My favorite football player is " + <footballer name>
* "His nationality is " + <nationality>
* "He plays for " + <football club>
*
* Author: Apisit Aphiphatthanaphokhin
* ID: 613040105-9
* Sec: 1
* Date: January 25, 2019
*
**/

package aphiphatthanaphokhin.apisit.lab2;

public class Footballer {
	public static void main(String[] args) {
		
		if( args.length == 3) {
		  System.out.println("My favorite football player is" +args[0]);
	      System.out.println("His nationality is" + args[1]);
	      System.out.println("He plays for" + args[2]);}
		
		else if( args.length != 3) {
			System.err.println("Usage: Footballer<footballer name><nationality><club name>  ");}
			      
	}
	
}
