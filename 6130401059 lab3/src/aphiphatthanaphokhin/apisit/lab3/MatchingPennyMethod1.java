/**
* This Java program defines three methods, namely acceptInput(), 
* genComChoice() and displayWiner() described below. 
* The program also contains two static String variable,
* namely humanChoice and compChoice to be used in the program methods. 
* 
* Author: Apisit Aphiphatthanaphokhin
* ID: 613040105-9
* Sec: 1
* Date: February 1, 2019
*
**/
package aphiphatthanaphokhin.apisit.lab3;

import java.util.Scanner;

public class MatchingPennyMethod1 {

	static String acceptInput() {
			@SuppressWarnings("resource")
			Scanner userinput = new Scanner(System.in);
			String checkHead = new String("head");
			String checkTail = new String("tail");
			String checkExit = new String("exit");
			System.out.print("Enter head and tail: ");
			String choice = userinput.nextLine();

			if ((choice.equalsIgnoreCase(checkHead) == true) || (choice.equalsIgnoreCase(checkTail) == true)) {

			} else if (choice.equalsIgnoreCase(checkExit)) {
				System.out.print("Good bye");
				System.exit(0);
			} else {
				System.err.print("Incorrect input. head or tail only \n ");

			}

			return choice;
		}

		static int genComChoice() {
			int random = (int) (Math.random() * 2);
			return random;

		}

		static void displaywinner(String choice, int random) {
			String checkHead = new String("head");
			String checkTail = new String("tail");
			if (random == 1) {
				System.out.println("Computer plays head");
			} else {
				System.out.println("Computer plays tail");
			}
			if (choice.equalsIgnoreCase(checkHead) && (random == 1)
					|| (choice.equalsIgnoreCase(checkTail) && (random == 0))) {
				System.out.println("You win.");
			} else {
				System.out.println("Computer win.");
			}
		}

		public static void main(String[] args) {
			while (true) {
				displaywinner(acceptInput(), genComChoice());
			}

		}

	}


