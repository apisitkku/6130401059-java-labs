/**
* This Java program that test whether a user types faster or slower than an average person. 
* An average person types at the speed of 40 words per minute. 
* The program randoms 8 colors from a list of rainbow colors 
* (RED, ORANGE, YELLOW, GREEN, BLUE, INDIGO, VIOLET). Color can be picked more than once. 
* A user must type in the same list ignoring case. If the user type in incorrectly, 
* he needs to type in again. After the correct input is enter, 
* the time used for typing in will be calculated and if he types in 
* faster than or equal to 12 seconds, he types faster than average otherwise 
* he types slower than average. The time for typing in can be computed using 
* subroutine currentTimeMillis in class System. 
* Note that the time for incorrect typing in will be added to the total typing time.
* 
* Author: Apisit Aphiphatthanaphokhin
* ID: 613040105-9
* Sec: 1
* Date: February 1, 2019
*
**/
package aphiphatthanaphokhin.apisit.lab3;

import java.util.Scanner;

	public class TypingTest1 {

		public static void main(String[] args) {
			String[] color = { "RED", "ORANGE", "YELLOW", "GREEN", "BLUE", "INDIGO", "VIOLET" };
			String colors = "";
			for (int i = 0; i < 8; i++) {
				int randomcolor = (int) (Math.random() * 7);

				colors += color[randomcolor] + " ";
			}

			System.out.println(colors);
			Scanner typing = new Scanner(System.in);
			Double timestart = (double) System.currentTimeMillis();
			while (true) {
				System.out.print("Type your answer:");
				String typingColors = typing.nextLine() + " ";
				if (colors.equalsIgnoreCase(typingColors)) {
					Double timestop = (double) System.currentTimeMillis();
					Double timenow = timestop - timestart;
					System.out.println("Your time is = " + (timenow) / 1000);
					if (timenow <= 12) {
						System.out.print("You type faster than average person");
						break;
					} else {
						System.out.print("You type slower than average person");
						System.exit(0);
					}
				} else {
					continue;
				}
			}
			typing.close();

		}

}


