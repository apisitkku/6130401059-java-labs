/**
* that will find the minimum, the maximum, the average and the standard deviation 
* of the list of numbers entered. The first argument is how many numbers to be entered. 
* The rest of the program arguments are list of numbers. Thus for program argument, 
* the program should check if there are numbers entered as many as the first integer.
* 
* Author: Apisit Aphiphatthanaphokhin
* ID: 613040105-9
* Sec: 1
* Date: February 1, 2019
*
**/
package aphiphatthanaphokhin.apisit.lab3;

public class BasicStat {
	static double[] number;
	static int inputNumber;
	static double aver;
	static double dev = 0;

	static void acceptInput(String[] args) {
		inputNumber = Integer.parseInt(args[0]);
		if (inputNumber == (args.length - 1)) {
			number = new double[args.length - 1];
			for (int i = 0; i < args.length - 1; i++) {
				number[i] = Double.parseDouble(args[i + 1]);
			}
		} else {
			System.err.println("<BasicStat> <numNumbers> <numbers>");
		}
	}

	static double[] minMax() {
		double min = number[0], max = number[0];
		for (int i = 0; i < inputNumber; i++) {
			double check = number[i];
			if (min > check) {
				min = check;
			}
			if (max > check) {
				max = check;
			}
		}
		double outputNum[] = new double[2];
		outputNum[0] = min;
		outputNum[1] = max;
		return outputNum;
	}

	static void findAverage() {
		double total = 0;
		for (int i = 0; i < inputNumber; i++) {
			total += number[i];
		}
		aver = total / inputNumber;
	}

	static void findStandardDeviation() {
		for (int i = 0; i < inputNumber; i++) {
			dev += Math.pow(number[i] - aver, 2);
		}
		dev /= inputNumber;
		dev = Math.sqrt(dev);
	}

	static void displayStats() {
		double[] minMaxInput = minMax();
		System.out.println("Max is "+minMaxInput[1]+"  Min is "+ minMaxInput[0]);

		findAverage();
		System.out.println("Average is "+ aver);

		findStandardDeviation();
		System.out.println("Standard Deviation is "+ dev);
	}

	public static void main(String[] args) {
		acceptInput(args);
		displayStats();
	}
}


