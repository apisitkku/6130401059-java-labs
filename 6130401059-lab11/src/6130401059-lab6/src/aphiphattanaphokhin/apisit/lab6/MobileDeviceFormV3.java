/**

 * 
 * Author: Apisit Aphiphatthanaphokhin
 * ID: 613040105-9
 * Sec: 1
 * Date: March 3, 2019
 *
 */
package aphiphattanaphokhin.apisit.lab6;

import java.awt.BorderLayout;
import java.awt.GridLayout;

import javax.swing.JLabel;
import javax.swing.JList;
import javax.swing.JMenu;
import javax.swing.JMenuBar;
import javax.swing.JMenuItem;
import javax.swing.JPanel;
import javax.swing.SwingUtilities;
import javax.swing.border.EmptyBorder;


public class MobileDeviceFormV3 extends MobileDeviceFormV2 {
	protected JLabel featuresLabel;
	protected JMenuBar menuBar;
	protected JMenu fileMenu,configMenu;
	protected JMenuItem newItem,openItem,saveItem,exitItem,colorItem,sizeItem;
	protected JList featuresList;
	protected JPanel featruresPanel;
	
	String[] featuresString = {"Design and build quality", "Great Camera", "Screen", "Battery Life"};
	
	public MobileDeviceFormV3(String title) {
		super(title);
	}
	protected void addComponents() {
		super.addComponents();
		JPanel featuresPanel = new JPanel();
		featuresPanel.setLayout(new GridLayout(0,2));
		JLabel featuresLabel = new JLabel("Features:");
		JList featuresList = new JList(featuresString);
		featuresPanel.add(featuresLabel);
		featuresPanel.add(featuresList);
		centerPanel.add(featuresPanel,BorderLayout.NORTH);
		centerPanel.setBorder(new EmptyBorder(10,0,0,0));
		
		
	}
	protected void addMenus() {
		JMenuBar menuBar = new JMenuBar();
		JMenu fileMenu = new JMenu("File");
		JMenu configMenu = new JMenu("Config");
		JMenuItem newItem = new JMenuItem("New");
		JMenuItem openItem = new JMenuItem("Open");
		JMenuItem saveItem = new JMenuItem("Save");
		JMenuItem exitItem = new JMenuItem("Exit");
		JMenuItem colorItem = new JMenuItem("Color");
		JMenuItem sizeItem = new JMenuItem("Size");
		fileMenu.add(newItem);
		fileMenu.add(openItem);
		fileMenu.add(saveItem);
		fileMenu.add(exitItem);
		configMenu.add(colorItem);
		configMenu.add(sizeItem);
		menuBar.add(fileMenu);
		menuBar.add(configMenu);
		this.setJMenuBar(menuBar);
	}
	protected void setFrameFeatures() {
		super.setFrameFeatures();
	}

	public static void createAndShowGUI() {
		MobileDeviceFormV3 msw = new MobileDeviceFormV3("MobileDeviceFromV3");
		msw.addComponents();
		msw.addMenus();
		msw.setFrameFeatures();
	}

	public static void main(String[] args) {
		SwingUtilities.invokeLater(new Runnable() {
			public void run() {
				createAndShowGUI();
			}
		});
	}
}

