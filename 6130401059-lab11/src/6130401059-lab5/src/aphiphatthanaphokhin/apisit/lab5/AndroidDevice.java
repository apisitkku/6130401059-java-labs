/**

 * 
 * Is a class created to be a super class used to define 
 * the specification of sub classes that will be created for real use
 * (AndroidSmartWatch)
 * 
 * Author: Apisit Aphiphatthanaphokhin
 * ID: 613040105-9
 * Sec: 1
 * Date: February 18, 2019
 *
 */
package aphiphatthanaphokhin.apisit.lab5;

public abstract class AndroidDevice {
	abstract void usage();

	private static String os = "Android";

	public String getOs() {
		return os;
	}

}
