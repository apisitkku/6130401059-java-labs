/**

 * 
 * This java program creates two objects that are instances 
 * of class AndroidDevice that is ticwatchPro and xiaomiMiBand3.
 * 
 * Author: Apisit Aphiphatthanaphokhin
 * ID: 613040105-9
 * Sec: 1
 * Date: February 18, 2019
 *
 */
package aphiphatthanaphokhin.apisit.lab5;

public class AndroidDevices2019 {
	public static void main(String[] args) {
		AndroidDevice ticwatchPro = new AndroidSmartWatch("Mobvoi", "TicWatch Pro", 8390);
		AndroidDevice xiaomiMiBand3 = new AndroidSmartWatch("Xiaomi", "Mi Band 3", 950);
		ticwatchPro.usage();
		System.out.println(ticwatchPro);
		System.out.println(xiaomiMiBand3);
	}
}
