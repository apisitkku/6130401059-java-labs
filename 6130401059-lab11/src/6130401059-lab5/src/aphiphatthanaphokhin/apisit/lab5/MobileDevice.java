/**

 * 
 * MobileDevice is to model a mobile device object with
 * attributes modelName, os, price, and weight. 
 * 
 * Author: Apisit Aphiphatthanaphokhin
 * ID: 613040105-9
 * Sec: 1
 * Date: February 18, 2019
 *
 */
package aphiphatthanaphokhin.apisit.lab5;

public class MobileDevice {
	private String modelName;
	private String os;
	private int price;
	private int weight;
	
	MobileDevice(String modelName,String os,int price,int weight){
		this.setModelName(modelName);
		this.setOs(os);
		this.setPrice(price);
		this.setWeight(weight);
	}
	MobileDevice(String modelName,String os,int price){
		this.setModelName(modelName);
		this.setOs(os);
		this.setPrice(price);
	}


	
	public String getModelName() {
		return modelName;
	}

	public void setModelName(String modelName) {
		this.modelName = modelName;
	}

	public String getOs() {
		return os;
	}

	public void setOs(String os) {
		this.os = os;
	}

	public int getPrice() {
		return price;
	}

	public void setPrice(int price) {
		this.price = price;
	}

	public int getWeight() {
		return weight;
	}

	public void setWeight(int weight) {
		this.weight = weight;
	}

	@Override
	public String toString() {
		return "MobileDevice [modelName=" + modelName + ", os=" + os + ", price=" + price + ", weight=" + weight + "]";
	}
	//The main function
		public static void main(String[] args) {

			}
}
