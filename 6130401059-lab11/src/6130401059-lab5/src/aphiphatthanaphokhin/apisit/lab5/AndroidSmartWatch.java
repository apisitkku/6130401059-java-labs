/**

 *
 * AndroidSmartwatch is to model a android smartwatch object with
 * attributes modelName, brandName and price. 
 * 
 * Author: Apisit Aphiphatthanaphokhin
 * ID: 613040105-9
 * Sec: 1
 * Date: February 18, 2019
 *
 */
package aphiphatthanaphokhin.apisit.lab5;

public class AndroidSmartWatch extends AndroidDevice {
	private String modelName;
	private String brandName;
	private int price;

	public AndroidSmartWatch(String brandName, String modelName, int price) {
		super();
		this.modelName = modelName;
		this.brandName = brandName;
		this.price = price;
	}
	
//	void displayTime(String brandName, String modelName, int price){
//		this.modelName = modelName;
//		this.brandName = brandName;
//		this.price = price;
//	}

	public String getModelName() {
		return modelName;
	}

	public void setModelName(String modelName) {
		this.modelName = modelName;
	}

	public String getBrandName() {
		return brandName;
	}

	public void setBrandName(String brandName) {
		this.brandName = brandName;
	}

	public int getPrice() {
		return price;
	}

	public void setPrice(int price) {
		this.price = price;
	}

	@Override
	public String toString() {
		return "AndroidSmartWatch [Brand Name :" + brandName + ", Model Name :" + modelName + ", Price :" + price + " Baht]";
	}

	@Override
	void usage() {
		System.out.println("AndroidSmartWatch Usage : Show time , date , your heart rate , and your step count");
		// TODO Auto-generated method stub
		
	}

	public void displayTime() {
		System.out.println("Display time only using a digital format");
		
	}

		
	}


		


	








