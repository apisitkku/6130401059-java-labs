package aphiphatthanaphokhin.apisit.lab10;

import java.awt.event.ActionEvent;
import java.awt.event.ActionListener;
import java.awt.event.ItemEvent;
import java.awt.event.ItemListener;

import javax.swing.JOptionPane;
import javax.swing.ListSelectionModel;
import javax.swing.SwingUtilities;
import javax.swing.event.ListSelectionEvent;
import javax.swing.event.ListSelectionListener;

import aphiphatthanaphokhin.apisit.lab7.MobileDeviceFormV6;



public class MobileDeviceFormV7 extends MobileDeviceFormV6
		implements ActionListener, ItemListener, ListSelectionListener {

	/**
	 * 
	 */
	private static final long serialVersionUID = 1L;

	public MobileDeviceFormV7(String title) {
		super(title);
		// TODO Auto-generated constructor stub
	}

	public static void main(String[] args) {
		// TODO Auto-generated method stub
		SwingUtilities.invokeLater(new Runnable() {
			public void run() {
				createAndShowGUI();
			}
		});
	}

	public static void createAndShowGUI() {
		// Mobile Device Form V7
		MobileDeviceFormV7 mobileDeviceFormV7 = new MobileDeviceFormV7("Mobile Device Form V7");
		mobileDeviceFormV7.addComponents();
		mobileDeviceFormV7.addMenus();
		mobileDeviceFormV7.initComponents();
		mobileDeviceFormV7.setFrameFeatures();
		mobileDeviceFormV7.addListeners();
	}

	@Override
	protected void addComponents() {
		super.addComponents();
		featureList.setSelectionMode(ListSelectionModel.MULTIPLE_INTERVAL_SELECTION);
		featureList.setSelectedIndices(new int[] { 0, 1, 3 });
	}

	@Override
	public void itemStateChanged(ItemEvent event) {
		// TODO Auto-generated method stub
		Object src = event.getSource();
		if (src == android || src == ios) {
			if (android.isSelected()) {
				JOptionPane.showMessageDialog(this, "Your os platform is now changed to Android");

			} else if (ios.isSelected()) {
				JOptionPane.showMessageDialog(this, "Your os platform is now changed to iOS");

			}

		} else if (event.getStateChange() == ItemEvent.SELECTED) {
			JOptionPane.showMessageDialog(this, "Type is updated to " + types.getSelectedItem());
		}

	}

	@Override
	public void actionPerformed(ActionEvent event) {
		// TODO Auto-generated method stub
		Object src = event.getSource();
		if (src == okButton) {
			handleOKButton();

		} else if (src == cancelButton) {
			handleCancelButton();
		}
	}

	private void handleCancelButton() {
		// TODO Auto-generated method stub
		reviewtextarea.setText("");
		brandNameTextField.setText("");
		modelNameTextField.setText("");
		weightTextField.setText("");
		priceTextField.setText("");
	}

	private void handleOKButton() {
		// TODO Auto-generated method stub
		if (android.isSelected()) {
			JOptionPane.showMessageDialog(this, "Brand Name: " + brandNameTextField.getText() + ", Model Name: "
					+ modelNameTextField.getText() + ", Weight: " + weightTextField.getText() + ", Price: "
					+ priceTextField.getText() + "\nOS: " + android.getText() + "\nType: "
					+ types.getSelectedItem() + "\nFeatures: " + featureList.getSelectedValuesList().toString()
							.substring(1, featureList.getSelectedValuesList().toString().length() - 1)
					+ "\nReview: " + reviewtextarea.getText());
		} else {
			JOptionPane.showMessageDialog(this, "Brand Name: " + brandNameTextField.getText() + ", Model Name: "
					+ modelNameTextField.getText() + ", Weight: " + weightTextField.getText() + ", Price: "
					+ priceTextField.getText() + "\nOS: " + ios.getText() + "\nType: "
					+ types.getSelectedItem() + "\nFeatures: " + featureList.getSelectedValuesList().toString()
							.substring(1, featureList.getSelectedValuesList().toString().length() - 1)
					+ "\nReview: " + reviewtextarea.getText());
		}

	}

	protected void addListeners() {
		okButton.addActionListener(this);
		cancelButton.addActionListener(this);
		android.addItemListener(this);
		ios.addItemListener(this);
		types.addItemListener(this);
		featureList.addListSelectionListener(this);
	}

	@Override
	public void valueChanged(ListSelectionEvent arg0) {
		// TODO Auto-generated method stub
		String features = "";
		Object obj[] = featureList.getSelectedValues();
		for (int i = 0; i < obj.length; i++) {
			if (obj.length == 1) {
				features += (String) obj[i];
			} else {
				if (i == obj.length - 1) {
					features += (String) obj[i];
				} else {
					features += (String) obj[i] + ", ";
				}
			}

		}
		JOptionPane.showMessageDialog(this, features);

	}
}
