package aphiphatthanaphokhin.apisit.lab10;

import java.awt.event.ActionEvent;
import java.awt.event.KeyEvent;

import javax.swing.JMenuItem;
import javax.swing.KeyStroke;
import javax.swing.SwingUtilities;

public class MobileDeviceFormV8 extends MobileDeviceFormV7 {
	/**
	 * 
	 */
	private static final long serialVersionUID = 1L;
	protected JMenuItem customMenu;

	public MobileDeviceFormV8(String title) {
		super(title);
		// TODO Auto-generated constructor stub
	}

	public static void main(String[] args) {
		// TODO Auto-generated method stub
		SwingUtilities.invokeLater(new Runnable() {
			public void run() {
				createAndShowGUI();
			}
		});
	}
	public static void createAndShowGUI() {
		// Mobile Device Form V8
		MobileDeviceFormV8 mobileDeviceFormV8 = new MobileDeviceFormV8("Mobile Device Form V8");
		mobileDeviceFormV8.addComponents();
		mobileDeviceFormV8.addMenus();
		mobileDeviceFormV8.initComponents();
		mobileDeviceFormV8.setFrameFeatures();
		mobileDeviceFormV8.addListeners();
		mobileDeviceFormV8.addShortcut();
	}
	
	@Override
	protected void addSubMenus() {
		super.addSubMenus();
		customMenu = new JMenuItem("Custom ...");
		colors.add(customMenu);
	}
	// add mnemonic key
	protected void addShortcut() {
		file.setMnemonic(KeyEvent.VK_F);
		configMenu.setMnemonic(KeyEvent.VK_C);
		news.setMnemonic(KeyEvent.VK_N);
		news.setAccelerator(KeyStroke.getKeyStroke(KeyEvent.VK_N,ActionEvent.ALT_MASK));
		open.setMnemonic(KeyEvent.VK_O);
		open.setAccelerator(KeyStroke.getKeyStroke(KeyEvent.VK_O,ActionEvent.ALT_MASK));
		save.setMnemonic(KeyEvent.VK_S);
		save.setAccelerator(KeyStroke.getKeyStroke(KeyEvent.VK_S,ActionEvent.ALT_MASK));
		exit.setMnemonic(KeyEvent.VK_X);
		exit.setAccelerator(KeyStroke.getKeyStroke(KeyEvent.VK_X,ActionEvent.ALT_MASK));
		color.setMnemonic(KeyEvent.VK_L);
		red.setMnemonic(KeyEvent.VK_R);
		red.setAccelerator(KeyStroke.getKeyStroke(KeyEvent.VK_R,ActionEvent.ALT_MASK));
		green.setMnemonic(KeyEvent.VK_G);
		green.setAccelerator(KeyStroke.getKeyStroke(KeyEvent.VK_G,ActionEvent.ALT_MASK));
		blue.setMnemonic(KeyEvent.VK_B);
		blue.setAccelerator(KeyStroke.getKeyStroke(KeyEvent.VK_B,ActionEvent.ALT_MASK));
		customMenu.setMnemonic(KeyEvent.VK_U);
		customMenu.setAccelerator(KeyStroke.getKeyStroke(KeyEvent.VK_U,ActionEvent.ALT_MASK));
		
	}
}