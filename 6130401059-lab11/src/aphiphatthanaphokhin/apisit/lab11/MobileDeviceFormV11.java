package aphiphatthanaphokhin.apisit.lab11;

import java.awt.event.ActionListener;

import javax.swing.JOptionPane;
import javax.swing.SwingUtilities;

public class MobileDeviceFormV11 extends MobileDeviceFormV10 implements ActionListener {

	/**
	 * 
	 */
	private static final long serialVersionUID = 1L;
	
	public MobileDeviceFormV11(String title) {
		super(title);
		// TODO Auto-generated constructor stub
	}
	
	public static void main(String[] args) {
		SwingUtilities.invokeLater(new Runnable() {
			public void run() {
				createAndShowGUI();
			}
		});
	}
	
	public static void createAndShowGUI() {
		MobileDeviceFormV11 MobileDeviceV11 = new MobileDeviceFormV11("MobileDeviceFormV11");
		MobileDeviceV11.addComponents();
		MobileDeviceV11.addMenus();
		MobileDeviceV11.setFrameFeatures();
		MobileDeviceV11.addListeners();
	}
	
	@Override
	public void handleOKButton() {
		// TODO Auto-generated method stub
		if (mNameTxtField.getText().equals("")) {
			JOptionPane.showMessageDialog(null, "Please enter model name");
		} else {
			try {
				Double weight = Double.parseDouble(weightTxtField.getText());
				if (weight < 100 ) {
					JOptionPane.showMessageDialog(null, "Too light: valid weight is [100,3000]");
				} else if (weight > 3000 ) {
					JOptionPane.showMessageDialog(null, "Too heavy: valid weight is [100,3000]");
				} else {
					super.handleOKButton();
				}
			} catch (NumberFormatException ex) {
				JOptionPane.showMessageDialog(null, "Please input only numeric input for weight");
			}
		}

		
	}
}

