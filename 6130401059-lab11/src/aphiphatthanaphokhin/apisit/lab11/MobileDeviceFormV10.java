package aphiphatthanaphokhin.apisit.lab11;

import java.awt.event.ActionEvent;
import java.awt.event.ActionListener;
import java.util.ArrayList;
import java.util.Collections;
import java.util.Comparator;

import javax.swing.JMenu;
import javax.swing.JMenuItem;
import javax.swing.JOptionPane;
import javax.swing.SwingUtilities;

import aphiphatthanaphokhin.apisit.lab10.MobileDeviceFormV9;
import aphiphatthanaphokhin.apisit.lab5.MobileDevice;

public class MobileDeviceFormV10 extends MobileDeviceFormV9 implements ActionListener {

	/**
	 * 
	 */
	private static final long serialVersionUID = 1L;

	protected MobileDevice mobileDevice;
	protected String searchName, removeName;

	protected ArrayList<MobileDevice> mobileDeviceList = new ArrayList<MobileDevice>();
	protected JMenu dataMenu;
	protected JMenuItem displayMenu, sortMenu, searchMenu, removeMenu;

	public MobileDeviceFormV10(String title) {
		super(title);
		// TODO Auto-generated constructor stub
	}

	public static void main(String[] args) {
		SwingUtilities.invokeLater(new Runnable() {
			public void run() {
				createAndShowGUI();
			}
		});
	}

	public void addMobileDevice(String os) {
		if (super.android.isSelected()) {
			os = "Android";
			mobileDevice = new MobileDevice(mNameTxtField.getText(), "Android",
					Integer.parseInt(priceTxtField.getText()), Integer.parseInt(weightTxtField.getText()));
			mobileDeviceList.add(mobileDevice);
			System.out.println(mobileDeviceList);
		} else {
			os = "iOS";
			mobileDevice = new MobileDevice(mNameTxtField.getText(), "iOS", Integer.parseInt(priceTxtField.getText()),
					Integer.parseInt(weightTxtField.getText()));
			mobileDeviceList.add(mobileDevice);
			System.out.println(mobileDeviceList);
		}
	}

	@Override
	public void handleOKButton() {
		// TODO Auto-generated method stub
		super.handleOKButton();
		addMobileDevice(toString());
	}

	@Override
	public void addMenus() {
		// TODO Auto-generated method stub
		super.addMenus();
		dataMenu = new JMenu("Data");
		displayMenu = new JMenuItem("Display");
		sortMenu = new JMenuItem("Sort");
		searchMenu = new JMenuItem("Search");
		removeMenu = new JMenuItem("Remove");

		menuBar.add(dataMenu);
		dataMenu.add(displayMenu);
		dataMenu.add(sortMenu);
		dataMenu.add(searchMenu);
		dataMenu.add(removeMenu);
	}

	protected void addComponents() {
		super.addComponents();
		addMenus();
	}

	@Override
	public void addListeners() {
		// TODO Auto-generated method stub
		super.addListeners();
		displayMenu.addActionListener(this);
		sortMenu.addActionListener(this);
		searchMenu.addActionListener(this);
		removeMenu.addActionListener(this);
	}

	@Override
	public void actionPerformed(ActionEvent e) {
		// TODO Auto-generated method stub
		super.actionPerformed(e);
		Object src = e.getSource();

		if (src == displayMenu) {
			showingMessage(toString());
		} else if (src == sortMenu) {
			Collections.sort(mobileDeviceList, new PriceComparator());
			showingMessage(toString());
		}

		else if (src == searchMenu) {
			searchName = JOptionPane.showInputDialog("Please input model name to search: ");
			int i = findingInList();
			if ((i) == -1) {
				JOptionPane.showMessageDialog(null, searchName + " is NOT found");
			} else {
				JOptionPane.showMessageDialog(null, mobileDeviceList.get(i));
			}
		}

		else if (src == removeMenu) {
			removeName = JOptionPane.showInputDialog("Please input model name to remove: ");
			int i = findingInList();
			if ((i) == -1) {
				JOptionPane.showMessageDialog(null, searchName + " is NOT found");
			} else {
				JOptionPane.showMessageDialog(null, mobileDeviceList.get(i) + " is removed.");
				mobileDeviceList.remove(i);
			}
		}
	}

	public void showingMessage(String os) {
		String outputMessage = "";
		if (super.android.isSelected()) {
			os = "Android";
			mobileDevice = new MobileDevice(mNameTxtField.getText(), os,
					Integer.parseInt(priceTxtField.getText()), Integer.parseInt(weightTxtField.getText()));
			mobileDeviceList.add(mobileDevice);
			for (int i = 0; i < mobileDeviceList.size(); i++) {
				outputMessage += String.valueOf(i + 1) + " : " + mobileDeviceList.get(i) + "\n\n";
			}
			JOptionPane.showMessageDialog(null, outputMessage);
		} else if(super.iOS.isSelected()) {
			os = "iOS";
			mobileDevice = new MobileDevice(mNameTxtField.getText(), os, Integer.parseInt(priceTxtField.getText()),
					Integer.parseInt(weightTxtField.getText()));
			mobileDeviceList.add(mobileDevice);
			for (int i = 0; i < mobileDeviceList.size(); i++) {
				outputMessage += String.valueOf(i + 1) + " : " + mobileDeviceList.get(i) + "\n\n";
			}
		}
	}

	public int findingInList() {
		for (int i = 0; i < mobileDeviceList.size(); i++) {
			if (mobileDeviceList.get(i).getModelName().equals(searchName)) {
				return i;
			}
		}
		return -1;
	}

	public static void createAndShowGUI() {
		MobileDeviceFormV10 MobileDeviceV10 = new MobileDeviceFormV10("MobileDeviceFormV10");
		MobileDeviceV10.addComponents();
		MobileDeviceV10.addMenus();
		MobileDeviceV10.setFrameFeatures();
		MobileDeviceV10.addListeners();
	}

	class PriceComparator implements Comparator<MobileDevice> {

		public int compare(MobileDevice o1, MobileDevice o2) {
			// TODO Auto-generated method stub
			return o1.getPrice() < o2.getPrice() ? -1 : o1.getPrice() == o2.getPrice() ? 0 : 1;
		}

	}
}