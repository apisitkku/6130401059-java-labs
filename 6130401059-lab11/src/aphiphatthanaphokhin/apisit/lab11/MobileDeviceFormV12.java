package aphiphatthanaphokhin.apisit.lab11;

import java.awt.event.ActionEvent;
import java.io.FileInputStream;
import java.io.FileOutputStream;
import java.io.IOException;
import java.io.ObjectInputStream;
import java.io.ObjectOutputStream;
import java.util.ArrayList;

import javax.swing.JFileChooser;
import javax.swing.SwingUtilities;

import aphiphatthanaphokhin.apis.lab5.MobileDevice;

public class MobileDeviceFormV12 extends MobileDeviceFormV11{

	/**
	 * 
	 */
	private static final long serialVersionUID = 1L;

	public MobileDeviceFormV12(String title) {
		super(title);
		// TODO Auto-generated constructor stub
	}
	
	public static void main(String[] args) {
		SwingUtilities.invokeLater(new Runnable() {
			public void run() {
				createAndShowGUI();
			}
		});
	}
	
	public static void createAndShowGUI() {
		MobileDeviceFormV12 MobileDeviceV12 = new MobileDeviceFormV12("MobileDeviceFormV12");
		MobileDeviceV12.addComponents();
		MobileDeviceV12.addMenus();
		MobileDeviceV12.setFrameFeatures();
		MobileDeviceV12.addListeners();
	}
	
	@SuppressWarnings("unchecked")
	@Override
	public void actionPerformed(ActionEvent e) {
		// TODO Auto-generated method stub
		super.actionPerformed(e);
		Object src = e.getSource();
		if (src == saveMI) {
			if (returnVal == JFileChooser.APPROVE_OPTION) {
//				file.getAbsoluteFile();
				
				try {
					FileOutputStream fos = new FileOutputStream(file);
					ObjectOutputStream oos = new ObjectOutputStream(fos);
					oos.writeObject(mobileDeviceList);
					oos.close();
					fos.close();
					
				} catch (IOException ex) {
					System.out.println(ex);
				}
			}
		} 
		else if (src == openMI) {
			if (returnVal == JFileChooser.APPROVE_OPTION) {
//				file.getAbsoluteFile();
				
				try {
					FileInputStream fis = new FileInputStream(file);
					ObjectInputStream ois = new ObjectInputStream(fis);
					mobileDeviceList = (ArrayList<MobileDevice>) ois.readObject();
					ois.close();
					fis.close();
					
				} catch (Exception ex) {
					System.out.println(ex);
				}
			}
		}
	}
}

