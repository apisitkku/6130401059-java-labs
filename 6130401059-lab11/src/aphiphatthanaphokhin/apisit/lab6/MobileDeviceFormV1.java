package aphiphatthanaphokhin.apisit.lab6;

import java.awt.*;

import javax.swing.*;

public class MobileDeviceFormV1 extends MySimpleWindow {

	private static final long serialVersionUID = 1L;
	protected JLabel mobileos;
	protected JLabel price;
	protected JLabel weight;
	protected JLabel modelName;
	protected JLabel brandName;
	protected JLabel mobileOS;
	protected JTextField priceTextField;
	protected JTextField weightTextField;
	protected JTextField modelNameTextField;
	protected JTextField brandNameTextField;
	protected JTextField mobileOSTextField;
	protected JRadioButton ios;
	protected JRadioButton android;
	protected ButtonGroup group;
	protected JPanel windowPanel;
	public int length = 10;

	public MobileDeviceFormV1(String title) {
		super(title);
	}

	protected void addComponents() {
		super.addComponents();
		
		android = new JRadioButton("Android");
		ios = new JRadioButton("ios");
		group = new ButtonGroup();
		group.add(android);
		group.add(ios);

		JPanel group = new JPanel();
		
		group.add(android);
		group.add(ios);
		
		windowPanel = new JPanel();
		windowPanel.setLayout(new GridLayout(0, 2));

		brandName = new JLabel("Brand name :");
		modelName = new JLabel("Model name :");
		weight = new JLabel("Weight (kg.) :");
		price = new JLabel("Price (baht) :");
		mobileOS = new JLabel("mobile OS :");

		brandNameTextField = new JTextField(length);
		modelNameTextField = new JTextField(length);
		weightTextField = new JTextField(length);
		priceTextField = new JTextField(length);
		mobileOSTextField = new JTextField(length);
		
		

		windowPanel.add(brandName);
		windowPanel.add(brandNameTextField);
		windowPanel.add(modelName);
		windowPanel.add(modelNameTextField);
		windowPanel.add(weight);
		windowPanel.add(weightTextField);
		windowPanel.add(price);
		windowPanel.add(priceTextField);
		
		windowPanel.add(group);

		JPanel panel = new JPanel();
		panel = (JPanel) getContentPane();
		panel.setLayout(new BorderLayout());
		panel.add(windowPanel, BorderLayout.CENTER);
		panel.add(selectPanel, BorderLayout.SOUTH);
		this.setContentPane(panel);

	}

	protected void setFrameFeatures() {
		super.setFrameFeatures();
	}

	public static void createAndShowGUI() {
		MobileDeviceFormV1 mdf = new MobileDeviceFormV1("Mobile Device Form V1");
		mdf.addComponents();
		mdf.setFrameFeatures();
		
		
	}

	public static void main(String[] args) {
		SwingUtilities.invokeLater(new Runnable() {
			public void run() {
				createAndShowGUI();
			}
		});
	}
}
