package aphiphatthanaphokhin.apisit.lab6;

import java.awt.*;
import javax.swing.*;

public class MobileDeviceFormV3 extends MobileDeviceFormV2 {
	private static final long serialVersionUID = 1L;
	protected JLabel features;
	protected JList<String> featureList;
	protected JPanel listPanel;
	protected JMenuBar menuBar;
	protected JMenu configMenu;
	protected JMenu file;
	protected JMenuItem news;
	protected JMenuItem open;
	protected JMenuItem save;
	protected JMenuItem exit;
	protected JMenuItem color;
	protected JMenuItem size;

	String[] listFeatures = { "Design and build quality", "Great Camera", "Screen", "Batter Life" };
	
	public MobileDeviceFormV3(String title) {
		super(title);
	}

	protected void addComponents() {
		super.addComponents();
		features = new JLabel("Features :");
		featureList = new JList<String>(listFeatures);
		JLabel freeLable = new JLabel();
		JLabel freeLables = new JLabel();
		JLabel freeLable2 = new JLabel();
		
		typePanel.add(freeLable);
		typePanel.add(freeLables);
		
		windowPanel.add(freeLable2);
		
		listPanel = new JPanel(new GridLayout());
		listPanel.add(features);
		listPanel.add(featureList);
		centerPanel.add(listPanel, BorderLayout.NORTH);
		

		JPanel panel = new JPanel();
		panel = (JPanel) getContentPane();
		panel.setLayout(new BorderLayout());
		panel.add(splityPanel, BorderLayout.NORTH);
		panel.add(centerPanel, BorderLayout.CENTER);
		panel.add(selectPanel, BorderLayout.SOUTH);
		this.setContentPane(panel);
	}

	protected void addMenus() {
		menuBar = new JMenuBar();

		file = new JMenu("File");

		news = new JMenuItem("New");
		open = new JMenuItem("Open");
		save = new JMenuItem("Save");
		exit = new JMenuItem("Exit");

		file.add(news);
		file.add(open);
		file.add(save);
		file.add(exit);

		configMenu = new JMenu("Config");
		color = new JMenuItem("Color");
		size = new JMenuItem("Size");

		configMenu.add(color);
		configMenu.add(size);
		menuBar.add(file);
		menuBar.add(configMenu);
		setJMenuBar(menuBar);
	}

	public static void createAndShowGUI() {
		MobileDeviceFormV3 mobileDeviceForm3 = new MobileDeviceFormV3("Mobile Device Form V3");
		mobileDeviceForm3.addComponents();
		mobileDeviceForm3.addMenus();
		mobileDeviceForm3.setFrameFeatures();
	}

	public static void main(String[] args) {
		SwingUtilities.invokeLater(new Runnable() {
			public void run() {
				createAndShowGUI();
			}
		});
	}
}