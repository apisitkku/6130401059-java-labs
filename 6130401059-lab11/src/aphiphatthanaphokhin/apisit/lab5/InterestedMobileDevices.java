/**

 * 
 * This java program creates two objects that are instances 
 * of class MobileDevice that is galaxyNote9 and iPadGen6
 * 
 * Author: Apisit Aphiphatthanaphokhin
 * ID: 613040105-9
 * Sec: 1
 * Date: February 18, 2019
 *
 */
package aphiphatthanaphokhin.apisit.lab5;

public class InterestedMobileDevices{
	public static void main(String[] args) {
		MobileDevice galaxyNote9 = new MobileDevice(" Galaxy Note 9", "Android", 25500, 201);
		MobileDevice iPadGen6 = new MobileDevice("Apple iPad Mini 3", "ios",11500);
		System.out.println(galaxyNote9);
		System.out.println(iPadGen6);
		iPadGen6.setPrice(11000);
		System.out.println(iPadGen6.getModelName() + " has new price as "
				+ iPadGen6.getPrice() + "Baht.");
		System.out.println(galaxyNote9.getModelName() + " has" + " weight as "
				+ galaxyNote9.getWeight() + " grams.");
	}
}
