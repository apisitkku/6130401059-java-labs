/**

 * 
 * This java program creates two objects that are instances 
 * of class AndroidSmartWatch that is ticwatchPro and note9.
 * 
 * Author: Apisit Aphiphatthanaphokhin
 * ID: 613040105-9
 * Sec: 1
 * Date: February 18, 2019
 *
 */
package aphiphatthanaphokhin.apisit.lab5;

public class TestPolymorphism {
	public static void main(String[] args) {
		
		AndroidSmartWatch ticwatchPro =
				new AndroidSmartWatch("Mobvoi","TicWatch Pro", 8390);
		SamsungDevice note9 = new SamsungDevice("Galaxy Note 9", 25500,201 ,801);
		
		System.out.println(ticwatchPro);
		ticwatchPro.displayTime();
		System.out.println();
		System.out.println(note9);
		note9.displayTime();
		
	}
	
}
