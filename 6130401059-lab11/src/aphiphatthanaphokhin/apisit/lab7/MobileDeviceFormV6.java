package aphiphatthanaphokhin.apisit.lab7;

import java.awt.*;

import javax.swing.*;

public class MobileDeviceFormV6 extends MobileDeviceFormV5 {

	private static final long serialVersionUID = 1L;
	protected JPanel splitPanel;

	public MobileDeviceFormV6(String title) {
		super(title);
	}

	protected void addComponents() {
		super.addComponents();

		ImageIcon myPicture = new ImageIcon("res/galaxyNote9.jpg");
		Image image = myPicture.getImage();
		Image newimg = image.getScaledInstance(450, 280, Image.SCALE_SMOOTH);
		ImageIcon images = new ImageIcon(newimg);

		JLabel picLabel = new JLabel(images);

		JPanel splitPanel = new JPanel(new BorderLayout());

		splitPanel.add(centerPanel, BorderLayout.NORTH);
		splitPanel.add(picLabel, BorderLayout.SOUTH);
		panel.add(splitPanel, BorderLayout.CENTER);
	}

	public static void createAndShowGUI() {

		MobileDeviceFormV6 mobileDeviceForm6 = new MobileDeviceFormV6("Mobile Device Form V6");
		mobileDeviceForm6.addComponents();
		mobileDeviceForm6.addMenus();
		mobileDeviceForm6.setFrameFeatures();
		mobileDeviceForm6.initComponents();

	}

	public static void main(String[] args) {
		SwingUtilities.invokeLater(new Runnable() {
			public void run() {
				createAndShowGUI();
			}
		});
	}
}