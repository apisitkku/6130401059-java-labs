/**
 *
 * 
 * Author: Apisit Aphiphatthanaphokhin
 * ID: 613040105-9
 * Sec: 1
 * Date: March 3, 2019
 *
 */
package aphiphatthanaphokhin.apisit.lab7;

import java.awt.BorderLayout;

import javax.swing.JComboBox;
import javax.swing.JLabel;
import javax.swing.JList;
import javax.swing.JPanel;
import javax.swing.JScrollPane;
import javax.swing.JTextArea;
import javax.swing.SwingUtilities;

public class MobileDeviceFormV2 extends MobileDeviceFormV1 {
	protected JComboBox typeBox;
	protected JLabel typeLabel, reviewLabel;
	protected JTextArea reviewArea;
	protected JPanel reviewPanel;
	protected JScrollPane scrollPanel;
	String[] typeStrings = { "Phone", "Tablet", "SmartTV" };

	public MobileDeviceFormV2(String title) {
		super(title);
	}

	protected void addComponents() {
		super.addComponents();
		typeBox = new JComboBox(typeStrings);
		typeLabel = new JLabel("Type:");
		reviewPanel = new JPanel();
		reviewPanel.setLayout(new BorderLayout());
		reviewLabel = new JLabel("Review:");
		reviewArea = new JTextArea(
				"Bigger than previous Note phones in every way, the Samsung Galaxy Note 9 has a larger 6.4-inch screen, heftier 4,000mAh battery, "
				+ "and a massive 1TB of storage option. ",2,35);
		reviewArea.setLineWrap(true);
		reviewArea.setWrapStyleWord(true);
		scrollPanel = new JScrollPane(reviewArea);
		scrollPanel.setVerticalScrollBarPolicy(JScrollPane.VERTICAL_SCROLLBAR_ALWAYS);
		
		contentPanel.add(typeLabel);
		contentPanel.add(typeBox);
		reviewPanel.add(reviewLabel,BorderLayout.NORTH);
		reviewPanel.add(scrollPanel,BorderLayout.SOUTH);
		centerPanel.add(reviewPanel,BorderLayout.SOUTH);

	}

	protected void setFrameFeatures() {
		super.setFrameFeatures();
	}

	public static void createAndShowGUI() {
		MobileDeviceFormV2 msw = new MobileDeviceFormV2("MobileDeviceFromV2");
		msw.addComponents();
		msw.setFrameFeatures();
	}

	public static void main(String[] args) {
		SwingUtilities.invokeLater(new Runnable() {
			public void run() {
				createAndShowGUI();
			}
		});
	}
}