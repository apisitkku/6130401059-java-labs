/**
 *
 * 
 * Author: Apisit Aphiphatthanaphokhin
 * ID: 613040105-9
 * Sec: 1
 * Date: March 3, 2019
 *
 */
package aphiphattanaphokhin.apisit.lab6;


import java.awt.BorderLayout;
import java.awt.GridLayout;

import javax.swing.*;

public class MySimpleWindow extends JFrame{
	protected JButton okButton;
	protected JButton cancelButton;
	protected JPanel windowPanel;
	protected JPanel buttonsPanel;
	protected JPanel contentPanel,centerPanel;
	protected JPanel radioButtonPanel;

	public MySimpleWindow(String title ) {
		super(title);
	}

	protected void addComponents() {
		cancelButton = new JButton("Cancel");
		okButton = new JButton("OK");
		buttonsPanel = new JPanel();
		buttonsPanel.add(cancelButton);
		buttonsPanel.add(okButton);
		contentPanel = new JPanel();
		windowPanel = new JPanel();
		centerPanel = new JPanel();
		centerPanel.setLayout(new BorderLayout());
		windowPanel = (JPanel) this.getContentPane();
		windowPanel.setLayout(new BorderLayout());
		windowPanel.add(contentPanel,BorderLayout.NORTH);
		windowPanel.add(centerPanel,BorderLayout.CENTER);
		windowPanel.add(buttonsPanel,BorderLayout.SOUTH);

	}

	protected void setFrameFeatures() {
		this.setLocationRelativeTo(null);
		pack();
		setVisible(true);
		this.setDefaultCloseOperation(JFrame.EXIT_ON_CLOSE);
	}

	public static void createAndShowGUI() {
		MySimpleWindow msw = new MySimpleWindow("MySimpleWindow");
		msw.addComponents();
		msw.setFrameFeatures();
	}

	public static void main(String[] args) {
		SwingUtilities.invokeLater(new Runnable() {
			public void run() {
				createAndShowGUI();
			}
		});
	}
}
