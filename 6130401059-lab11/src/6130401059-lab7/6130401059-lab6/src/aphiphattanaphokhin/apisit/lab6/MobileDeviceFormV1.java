/**

 * 
 * Author: Apisit Aphiphatthanaphokhin
 * ID: 613040105-9
 * Sec: 1
 * Date: March 3, 2019
 *
 */
package aphiphattanaphokhin.apisit.lab6;

import java.awt.BorderLayout;
import java.awt.GridLayout;

import javax.swing.*;

public class MobileDeviceFormV1 extends MySimpleWindow{
	 
		protected JLabel nameLabel, modelLabel, weightLabel, priceLabel, osLabel;
		protected JTextField nameText, modelText, weightText, priceText;
		protected JRadioButton iosButton,androidButton;

		public MobileDeviceFormV1(String title) {
			super(title);
			// TODO Auto-generated constructor stub
		}

		protected void addComponents() {
			super.addComponents();
			contentPanel.setLayout(new GridLayout(0, 2,5,5));
			radioButtonPanel = new JPanel();
			androidButton = new JRadioButton("Android");
			iosButton = new JRadioButton("iOS");
			ButtonGroup group = new ButtonGroup();
			group.add(androidButton);
			group.add(iosButton);
			radioButtonPanel.add(androidButton);
			radioButtonPanel.add(iosButton);
			nameLabel = new JLabel("Brand Name: ");
			modelLabel = new JLabel("Model Name: ");
			weightLabel = new JLabel("Weight (kg.) : ");
			priceLabel = new JLabel("Price (Baht): ");
			osLabel = new JLabel("Mobile OS:");
			nameText = new JTextField(15);
			modelText = new JTextField(15);
			weightText = new JTextField(15);
			priceText = new JTextField(15);

			contentPanel.add(nameLabel);
			contentPanel.add(nameText);
			contentPanel.add(modelLabel);
			contentPanel.add(modelText);
			contentPanel.add(weightLabel);
			contentPanel.add(weightText);
			contentPanel.add(priceLabel);
			contentPanel.add(priceText);
			contentPanel.add(osLabel);
			contentPanel.add(radioButtonPanel);
		
		}
		
		protected void setFrameFeatures() {
			super.setFrameFeatures();
		}
		public static void createAndShowGUI() {
			MobileDeviceFormV1 msw = new MobileDeviceFormV1("MobileDeviceFromV1");
			msw.addComponents();
			msw.setFrameFeatures();
		}

		public static void main(String[] args) {
			SwingUtilities.invokeLater(new Runnable() {
				public void run() {
					createAndShowGUI();
				}
			});
		}
}
