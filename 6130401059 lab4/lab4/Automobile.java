/**

 * 
 * Author: Apisit Aphiphatthanaphokhin
 * ID: 613040105-9
 * Sec: 1
 * Date: February 10, 2019
 *
 */
package aphiphatthanaphokhin.apisit.lab4;

public class Automobile {
	
	//This is for create a variable that will use in this program.
	private int gasoline, speed, maxSpeed, acceleration;
	private String model;
	static int numberOfAutomobile = 0;
	private Color color;
	enum Color {
		RED, ORANGE, YELLOW, GREEN, BLUE, INDIGO, VIOLET, WHITE , BLACK
		}
	
	
	//Contractor for making a Automobile with the inputed variable.
	Automobile(int gasoline,int speed,int maxSpeed,int acceleration, String model, Color color) {
		this.gasoline = gasoline;
		this.speed = speed;
		this.maxSpeed = maxSpeed;
		this.acceleration = acceleration;
		this.model = model;
		this.color = color;
		numberOfAutomobile += 1;
		}
	
	
	//Empty Contractor for making a Automobile with no input.
	Automobile() {
		this.setGasoline(0);
		this.setThisSpeed(0);
		this.setMaxSpeed(160);
		this.setAcceleration(0);
		this.setModel("Automobile");
		this.setColor(Color.WHITE);
		numberOfAutomobile += 1;
		}
	
	
	//All the getter setter.
	public void setGasoline(int gasoline) {
		this.gasoline = gasoline;
		}
	public void setThisSpeed(int speed) {
		this.speed = speed;
		}
	public void setMaxSpeed(int maxSpeed) {
		this.maxSpeed = maxSpeed;
		}
	public void setAcceleration(int acceleration) {
		this.acceleration = acceleration;
		}
	public void setModel(String model) {
		this.model = model;
		}
	public void setColor(Color color) {
		this.color = color;
		}
	public int getGasoline() {
		return gasoline;
		}
	public int getSpeed() {
		return speed;
		}
	public int getMaxSpeed() {
		return maxSpeed;
		}
	public int getAcceleration() {
		return acceleration;
		}
	public String getModel() {
		return model;
		}
	public Color getColor() {
		return color;
		}
	
	
	//This function were made to count how many Automobile were made.
	public static int getNumberOfAutomobile() {
		return numberOfAutomobile;
		}
	
	
	//This function is use to override a print Automobile template.
	public String toString() {
		return String.format("Automobile [gasoline=%s, speed=%s, maxSpeed=%s, acceleration=%s, model=%s, color=%s]"
				, gasoline, speed, maxSpeed, acceleration, model, color);
		}
	
	
	//The main function
	public static void main(String[] args) {

		}
	
}