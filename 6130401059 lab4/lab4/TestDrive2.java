/**

 * 
 * Author: Apisit Aphiphatthanaphokhin
 * ID: 613040105-9
 * Sec: 1
 * Date: February 10, 2019
 *
 */
package aphiphatthanaphokhin.apisit.lab4;

public class TestDrive2  {
	
	
	//this function is create to compare 2 car's speed
	public static void isFaster(Automobile car1, Automobile car2) {
		if (car1.getSpeed() > car2.getSpeed()) {
			System.out.printf("%s is faster than %s\n", car1.getModel(), car2.getModel());	
		}
		else {
			System.out.printf("%s is Not faster than %s\n", car1.getModel(), car2.getModel());
			
		}
		
	}
	
	
	//the main function
	public static void main(String[] atgs) {
		ToyotaAuto car1 = new ToyotaAuto(200, 10, "Vios");
		HondaAuto car2 = new HondaAuto(220, 8, "City");
		
		System.out.println(car1);
		System.out.println(car2);
		
		car1.accelerate();
		car2.accelerate();
		car2.accelerate();
		
		System.out.println(car1);
		System.out.println(car2);
		
		car1.brake();
		car1.brake();
		car2.brake();
		
		System.out.println(car1);
		System.out.println(car2);
		
		car1.refuel();
		car2.refuel();
		System.out.println(car1);
		System.out.println(car2);
		isFaster(car1, car2);
		isFaster(car2, car1);
	}
}
