/**

 * 
 * Author: Apisit Aphiphatthanaphokhin
 * ID: 613040105-9
 * Sec: 1
 * Date: February 10, 2019
 *
 */

package aphiphatthanaphokhin.apisit.lab4;

public class ToyotaAuto extends Automobile implements Movable, Refuelable {

	
	//Contractor for making a Automobile with the inputed variable.
	ToyotaAuto(int maxSpeed, int acceleration, String model) {
		this.setMaxSpeed(maxSpeed);
		this.setAcceleration(acceleration);;
		this.setModel(model);
		this.setGasoline(100);
		}
	
	
	//Function for set Gasoline variable to 100.
	public void refuel() {
		this.setGasoline(100);
		System.out.printf("%s refuels\n", this.getModel());
	}

	
	//This function is use to override a print Automobile (ToyotaAuto) template.
	@Override
	public String toString() {
		return String.format("%s gas:%s speed:%s max speed:%s acceleration:%s"
				, this.getModel(), this.getGasoline(), this.getSpeed(), this.getMaxSpeed(), this.getAcceleration());
		}
	
	
	/*this function is to increases the current speed by the acceleration with a condition.
	  If the speed is higher than max speed it will set speed to max speed*/
	public void accelerate() {
		if (this.getSpeed() + this.getAcceleration() <= this.getMaxSpeed()) {
			this.setSpeed(this.getSpeed() + this.getAcceleration());
			this.setGasoline(this.getGasoline() - 15);
			}
		else {
			this.setSpeed(this.getMaxSpeed());
			this.setGasoline(this.getGasoline() - 15);
			}
		System.out.printf("%s accelerates\n", this.getModel());
		}
	
	
	/*this function is to decreases the current speed by the acceleration with a condition.
	  If the speed is lower than 0 it will set speed to 0*/
	public void brake() {
		if (this.getSpeed() - this.getAcceleration() >= 0) {
			this.setSpeed(this.getSpeed() - this.getAcceleration());
			this.setGasoline(this.getGasoline() - 15);
			}
		else {
			this.setSpeed(0);
			this.setGasoline(this.getGasoline() - 15);
			}
		System.out.printf("%s brakes\n", this.getModel());
		}

	
	/*This function will set car speed by the input when input is in range (0, max speed)
	  if not it will set to 0 or max speed depends on it lower or higher than range. */
	public void setSpeed(int speed) {
		if (speed < 0) {
			this.setThisSpeed(0);
			}
		
		else if (speed > this.getMaxSpeed()) {
			this.setThisSpeed(this.getMaxSpeed());;
			}	
		
		else {
			this.setThisSpeed(speed);
			}
	}
	
	
	//The main function
	public static void main(String[] args) {
		
	}

}
	
