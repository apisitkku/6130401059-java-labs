/**

 * 
 * Author: Apisit Aphiphatthanaphokhin
 * ID: 613040105-9
 * Sec: 1
 * Date: February 10, 2019
 *
 */
package aphiphatthanaphokhin.apisit.lab4;


//Interface of Refuelable
public interface Refuelable {
	
	public void refuel();
	
}
