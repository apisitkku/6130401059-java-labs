package aphiphatthanaphokhin.apisit.lab6;

import java.awt.*;

import javax.swing.*;

public class MySimpleWindow extends JFrame {

	private static final long serialVersionUID = 1L;
	protected JButton okButton;
	protected JButton cancelButton;
	protected JPanel panel;
	protected JPanel selectPanel;

	public MySimpleWindow(String title) {
		super(title);
	}

	protected void setFrameFeatures() {
		this.setLocationRelativeTo(null);
		pack();
		setVisible(true);
		setDefaultCloseOperation(JFrame.EXIT_ON_CLOSE);
	}

	protected void addComponents() {
		cancelButton = new JButton("Cancel");
		okButton = new JButton("  OK  ");
		selectPanel = new JPanel();
		selectPanel.add(cancelButton);
		selectPanel.add(okButton);
		panel = (JPanel) getContentPane();
		panel.setLayout(new BorderLayout());
		panel.add(selectPanel, BorderLayout.SOUTH);
		this.setContentPane(panel);
	}

	public static void createAndShowGUI() {
		MySimpleWindow msw = new MySimpleWindow("My Simple Window");
		msw.addComponents();
		msw.setFrameFeatures();
	}

	public static void main(String[] args) {
		SwingUtilities.invokeLater(new Runnable() {
			public void run() {
				createAndShowGUI();
			}
		});
	}
}
