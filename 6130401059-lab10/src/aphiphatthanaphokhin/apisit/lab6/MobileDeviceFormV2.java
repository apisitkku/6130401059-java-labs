package aphiphatthanaphokhin.apisit.lab6;

import java.awt.*;
import javax.swing.*;

public class MobileDeviceFormV2 extends MobileDeviceFormV1 {

	protected JLabel review;
	protected JLabel type;
	protected JComboBox<String> types;
	protected JTextArea reviewtextarea;
	protected JPanel centerPanel;
	protected JPanel splityPanel;
	protected JPanel typePanel;
	protected JPanel typesPanel;
	protected JScrollPane scrollPane;
	private static final long serialVersionUID = 1L;

	public MobileDeviceFormV2(String title) {
		super(title);
	}

	protected void addComponents() {
		super.addComponents();

		String[] typing = new String[] { "Phone", "Tablet", "Smart TV" };
		types = new JComboBox<String>(typing);
		JLabel typeLable = new JLabel("Type :");
		JLabel reviewLable = new JLabel("Review :");
		
		splityPanel = new JPanel();
		splityPanel = new JPanel(new BorderLayout());
		
		typePanel = new JPanel();
		typesPanel = new JPanel();
		
		typePanel.setLayout(new GridLayout(0, 2));
		
		typePanel.add(typeLable);
		typePanel.add(types);
		
		splityPanel.add(windowPanel, BorderLayout.NORTH);
		splityPanel.add(typePanel, BorderLayout.SOUTH);
		

		centerPanel = new JPanel(new BorderLayout());
		reviewtextarea = new JTextArea(2, 35);
		JScrollPane scrollPane = new JScrollPane(reviewtextarea);
		reviewtextarea.setEditable(true);
		reviewtextarea.setText(
				"Bigger than previous Note phones in every way, the Samsung \nGalaxy Note 9 has a larger 6.4-inch screen, heftier 4,000mAh \nbattery, and a massive 1TB of storage option. ");
		centerPanel.add(scrollPane, BorderLayout.SOUTH);
		centerPanel.add(reviewLable, BorderLayout.CENTER);

		JPanel panel = new JPanel();
		panel = (JPanel) getContentPane();
		panel.setLayout(new BorderLayout());
		panel.add(splityPanel, BorderLayout.NORTH);
		panel.add(centerPanel, BorderLayout.CENTER);
		panel.add(selectPanel, BorderLayout.SOUTH);
		this.setContentPane(panel);
	}

	public static void createAndShowGUI() {
		MobileDeviceFormV2 mobileDeviceForm2 = new MobileDeviceFormV2("Mobile Device Form V2");
		mobileDeviceForm2.addComponents();
		mobileDeviceForm2.setFrameFeatures();
	}

	public static void main(String[] args) {
		SwingUtilities.invokeLater(new Runnable() {
			public void run() {
				createAndShowGUI();
			}
		});
	}

}
