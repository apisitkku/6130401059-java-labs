package aphiphatthanaphokhin.apisit.lab7;

import javax.swing.*;
import java.awt.*;

public class MobileDeviceFormV5 extends MobileDeviceFormV4 {

	private static final long serialVersionUID = 1L;

	public MobileDeviceFormV5(String title) {
		super(title);
	}

	
	protected void addComponents() {
		super.addComponents();
		okButton.setForeground(Color.BLUE);
		cancelButton.setForeground(Color.RED);
	}

	protected void initComponents() {

		brandName.setFont(new Font("Serif", Font.PLAIN, 14));
		modelName.setFont(new Font("Serif", Font.PLAIN, 14));
		weight.setFont(new Font("Serif", Font.PLAIN, 14));
		price.setFont(new Font("Serif", Font.PLAIN, 14));
		mobileOS.setFont(new Font("Serif", Font.PLAIN, 14));

		brandNameTextField.setFont(new Font("Serif", Font.BOLD, 14));
		modelNameTextField.setFont(new Font("Serif", Font.BOLD, 14));
		weightTextField.setFont(new Font("Serif", Font.BOLD, 14));
		priceTextField.setFont(new Font("Serif", Font.BOLD, 14));
		mobileOSTextField.setFont(new Font("Serif", Font.BOLD, 14));

	}

	public static void createAndShowGUI() {
		
		MobileDeviceFormV5 mobileDeviceForm5 = new MobileDeviceFormV5("Mobile Device Form V5");
		mobileDeviceForm5.addComponents();
		mobileDeviceForm5.addMenus();
		mobileDeviceForm5.setFrameFeatures();
		mobileDeviceForm5.initComponents();
		
	}

	public static void main(String[] args) {
		SwingUtilities.invokeLater(new Runnable() {
			public void run() {
				createAndShowGUI();
			}
		});
	}
}