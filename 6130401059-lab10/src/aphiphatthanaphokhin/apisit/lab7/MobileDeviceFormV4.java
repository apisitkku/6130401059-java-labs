package aphiphatthanaphokhin.apisit.lab7;

import javax.swing.*;

import aphiphatthanaphokhin.apisit.lab6.MobileDeviceFormV3;

public class MobileDeviceFormV4 extends MobileDeviceFormV3 {
	
	
	private static final long serialVersionUID = 1L;
	protected JMenuItem red;
	protected JMenuItem green;
	protected JMenuItem blue;
	protected JMenuItem s16;
	protected JMenuItem s20;
	protected JMenuItem s24;
	protected JMenu sizes;
	protected JMenu colors;
	
	public MobileDeviceFormV4(String title) {
		super(title);
	}

	protected void updateMenuIcon() {
		news.setIcon(new ImageIcon("res/new.jpg"));

	}

	protected void addSubMenus() {

		colors = new JMenu("Color");
		sizes = new JMenu("Size");
		
		red = new JMenuItem("Red");
		green = new JMenuItem("Green");
		blue = new JMenuItem("Blue");
		
		colors.add(red);
		colors.add(green);
		colors.add(blue);
		
		s16 = new JMenuItem("16");
		s20 = new JMenuItem("20");
		s24 = new JMenuItem("24");
		
		sizes.add(s16);
		sizes.add(s20);
		sizes.add(s24);
		
		configMenu.remove(color);
		configMenu.remove(size);
		configMenu.add(colors);
		configMenu.add(sizes);
		
		this.setJMenuBar(menuBar);

	}

	protected void setFrameFeatures() {
		super.setFrameFeatures();
	}

	protected void addComponents() {
		super.addComponents();
	}

	protected void addMenus() {
		super.addMenus();
		updateMenuIcon();
		addSubMenus();

	}

	public static void createAndShowGUI() {
		MobileDeviceFormV4 mobileDeviceFormV4 = new MobileDeviceFormV4("MobileDeviceFromV4");
		mobileDeviceFormV4.addComponents();
		mobileDeviceFormV4.addMenus();
		mobileDeviceFormV4.setFrameFeatures();
	}

	public static void main(String[] args) {
		SwingUtilities.invokeLater(new Runnable() {
			public void run() {
				createAndShowGUI();
			}
		});
	}
}