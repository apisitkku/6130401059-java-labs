/**
 *
 * 
 * Author: Apisit Aphiphatthanaphokhin
 * ID: 613040105-9
 * Sec: 1
 * Date: April 2, 2019
 *
 */
package aphiphatthanaphokhin.apisit.lab8;

import java.awt.BasicStroke;
import java.awt.Color;
import java.awt.Component;
import java.awt.Graphics;
import java.awt.Graphics2D;

public class MyCanvasV4 extends MyCanvasV3 {
	
	@Override
	public void paint(Graphics g){
		super.paint(g);
		Graphics2D g2d = (Graphics2D)g;
		g2d.setColor(Color.black);
		g2d.fillRect(0, 0, WIDTH, HEIGHT);
		MyBrick brick[] = new MyBrick[10];
		Color brickRow[] = new Color[7];
		brickRow[0] = Color.red;
		brickRow[1] = Color.orange;
		brickRow[2] = Color.yellow;
		brickRow[3] = Color.green;
		brickRow[4] = Color.cyan;
		brickRow[5] = Color.blue;
		brickRow[6] = Color.magenta;
		
		g2d.setStroke(new BasicStroke(4));
		int x = 0;
		int y = 200;
		
		for (int i = 0; i < brickRow.length; i++) {
			for (int j = 0; j < brick.length; j++) {
				g2d.setColor(brickRow[6-i]);
				brick[j] = new MyBrick(x,y);
				x += 80;
				g2d.fill(brick[j]);
				g2d.setColor(Color.black);
				g2d.draw(brick[j]);
			}
			x = 0;
			y -= 20;
		}
		
		g2d.setColor(Color.gray);
		pedal = new MyPedal(350,590);
		g2d.fill(pedal);
		g2d.setColor(Color.white);
		ball = new MyBall(385,560);
		g2d.fill(ball);
			
	}
}


