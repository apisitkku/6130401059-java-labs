/**
 *
 * 
 * Author: Apisit Aphiphatthanaphokhin
 * ID: 613040105-9
 * Sec: 1
 * Date: April 6, 2019
 *
 */
package aphiphatthanaphokhin.apisit.lab9;

import java.awt.Color;

import aphiphatthanaphokhin.apisit.lab8.MyBall;
import aphiphatthanaphokhin.apisit.lab8.MyCanvas;
import aphiphatthanaphokhin.apisit.lab8.MyCanvasV4;

public class MyCanvasV5 extends  MyCanvasV4 implements Runnable{
	MyBallV2 ball = new MyBallV2(0,  MyCanvas.HEIGHT/2 - MyBall.diameter/2);
	protected Thread running = new Thread(this);
	
	public MyCanvasV5() {
		super();
		ball.ballVelX =2 ;
		ball.ballVelY =0 ;
		running.start() ;
	}

	@Override
	public void run() {
		while(true) {
			
			if(ball.x + ball.diameter > MyCanvas.WIDTH) {
				ball.ballVelX *= 0 ;
			}
			
			
			ball.move() ;
			
			repaint();
			
			try {
				Thread.sleep(10);
			}
			catch(InterruptedException ex) {
				
			}
		}
		
	}
}
	
