/**
 *
 * 
 * Author: Apisit Aphiphatthanaphokhin
 * ID: 613040105-9
 * Sec: 1
 * Date: April 6, 2019
 *
 */
package aphiphatthanaphokhin.apisit.lab9;

import aphiphatthanaphokhin.apisit.lab8.MyBrick;

public class MyBrickV2 extends MyBrick{
	protected boolean visible ;

	public MyBrickV2(int x, int y) {
		super(x, y);
		visible = true ;
	}

}
