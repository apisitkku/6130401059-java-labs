/**
 *
 * 
 * Author: Apisit Aphiphatthanaphokhin
 * ID: 613040105-9
 * Sec: 1
 * Date: April 6, 2019
 *
 */
package aphiphatthanaphokhin.apisit.lab9;

import aphiphatthanaphokhin.apisit.lab8.MyBall;

public class MyBallV2 extends MyBall {

	public MyBallV2(int x, int y) {
		super(x, y);
		// TODO Auto-generated constructor stub
	}

	protected int ballVelX , ballVelY;
	
	public void move() {
		x += ballVelX ;
		y += ballVelY ;
	}

}