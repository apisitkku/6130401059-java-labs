/**
 *
 * 
 * Author: Apisit Aphiphatthanaphokhin
 * ID: 613040105-9
 * Sec: 1
 * Date: April 6, 2019
 *
 */
package aphiphatthanaphokhin.apisit.lab9;

import javax.swing.SwingUtilities;

import aphiphatthanaphokhin.apisit.lab8.MyCanvasV4;
import aphiphatthanaphokhin.apisit.lab8.MyFrameV4;

public class MyFrameV5 extends MyFrameV4 {

	public MyFrameV5(String string) {
		super(string);
		// TODO Auto-generated constructor stub
	}
	public static void main(String[] args) {
		SwingUtilities.invokeLater(new Runnable() {
			public void run() {
				createAndShowGUI();
			}
		});

	}

	public static void createAndShowGUI() {
		MyFrameV5 msw = new MyFrameV5("My Frame V5");
		msw.addComponents();
		msw.setFrameFeatures();
		
	}
	protected void addComponents() {
		add(new MyCanvasV5());
	}
}
