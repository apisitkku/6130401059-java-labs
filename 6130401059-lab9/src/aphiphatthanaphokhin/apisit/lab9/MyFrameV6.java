/**
 *
 * 
 * Author: Apisit Aphiphatthanaphokhin
 * ID: 613040105-9
 * Sec: 1
 * Date: April 6, 2019
 *
 */
package aphiphatthanaphokhin.apisit.lab9;

import javax.swing.SwingUtilities;

public class MyFrameV6 extends MyFrameV5{
	
	public MyFrameV6(String string) {
		super(string);
		// TODO Auto-generated constructor stub
	}
	public static void main(String[] args) {
		SwingUtilities.invokeLater(new Runnable() {
			public void run() {
				createAndShowGUI();
			}
		});

	}

	public static void createAndShowGUI() {
		MyFrameV6 msw = new MyFrameV6("My Frame V6");
		msw.addComponents();
		msw.setFrameFeatures();
		
	}
	protected void addComponents() {
		add(new MyCanvasV6());
	}
}