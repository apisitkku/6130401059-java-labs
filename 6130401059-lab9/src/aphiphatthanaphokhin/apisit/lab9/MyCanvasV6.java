/**
 *
 * 
 * Author: Apisit Aphiphatthanaphokhin
 * ID: 613040105-9
 * Sec: 1
 * Date: April 6, 2019
 *
 */
package aphiphatthanaphokhin.apisit.lab9;

import java.awt.Color;
import java.awt.Component;
import java.awt.Graphics;
import java.awt.Graphics2D;
import java.awt.geom.Rectangle2D;

import aphiphatthanaphokhin.apisit.lab8.MyBall;
import aphiphatthanaphokhin.apisit.lab8.MyCanvas;

public class MyCanvasV6 extends MyCanvasV5 implements Runnable{
	MyBallV2 ball = new MyBallV2(MyCanvas.WIDTH/2 - MyBall.diameter/2 ,  MyCanvas.HEIGHT/2 - MyBall.diameter/2);
	protected Thread running = new Thread(this);
	
	public MyCanvasV6() {
		super();
		ball.ballVelX =1 ;
		ball.ballVelY =1 ;
		running.start() ;
	}
	
	public void run() {
		while(true) {		
			if(ball.x <= 0 || ball.x + MyBall.diameter >= MyCanvas.WIDTH - 30) {
				ball.ballVelX *= -1 ;
			}	
			
			if(ball.y <= 0 || ball.y + MyBall.diameter >= MyCanvas.HEIGHT - 30) {
				ball.ballVelY *= -1 ;
			}			
			
			ball.move() ;
			
			repaint();
			
			try {
				Thread.sleep(10);
			}
			catch(InterruptedException ex) {
				
			}
		}
		
	}
	
	public void paint(Graphics g) {
		super.paint(g);
		Graphics2D g2d = (Graphics2D) g;
		g2d.setColor(Color.BLACK);
		g2d.fill(new Rectangle2D.Double(0, 0, super.WIDTH, super.HEIGHT));
		g2d.setColor(Color.WHITE);
		g2d.fill(ball);
	}
	
	
	
	
	
}

