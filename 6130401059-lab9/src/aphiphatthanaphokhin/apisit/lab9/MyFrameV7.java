/**
 *
 * 
 * Author: Apisit Aphiphatthanaphokhin
 * ID: 613040105-9
 * Sec: 1
 * Date: April 6, 2019
 *
 */
package aphiphatthanaphokhin.apisit.lab9;

import javax.swing.SwingUtilities;

public class MyFrameV7 extends MyFrameV6 {

	public MyFrameV7(String string) {
		super(string);
		// TODO Auto-generated constructor stub
	}
	public static void main(String[] args) {
		SwingUtilities.invokeLater(new Runnable() {
			public void run() {
				createAndShowGUI();
			}
		});

	}

	public static void createAndShowGUI() {
		MyFrameV7 msw = new MyFrameV7("My Frame V7");
		msw.addComponents();
		msw.setFrameFeatures();
		
	}
	protected void addComponents() {
		add(new MyCanvasV7());
	}
}