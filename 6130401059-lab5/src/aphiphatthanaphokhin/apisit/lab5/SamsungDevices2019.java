/**

 * 
 * This java program creates two objects that are instances 
 * of class SamsungDevice that is s9 and note9.
 * 
 * Author: Apisit Aphiphatthanaphokhin
 * ID: 613040105-9
 * Sec: 1
 * Date: February 18, 2019
 *
 */
package aphiphatthanaphokhin.apisit.lab5;

public class SamsungDevices2019 {
	public static void main(String[] args) {
		MobileDevice s9 = new SamsungDevice(" Galaxy S9", 23900, 163, 8.0);
		SamsungDevice note9 = new SamsungDevice(" Galaxy Note 9", 25500, 201, 8.1);
		System.out.println("I would like to have ");
		System.out.println(note9);
		System.out.println("But to save money, I should buy ");
		System.out.println(s9);
		double diff = note9.getPrice() - s9.getPrice();
		System.out.println("Samsung Galaxy Note 9 is more expensive"
				+ " than Samsung Galaxy S9 by "
				+ diff + " Baht.");
		System.out.println("Both these devices have the same brand which is"
				+ SamsungDevice.getBrand());
		
	}
}
