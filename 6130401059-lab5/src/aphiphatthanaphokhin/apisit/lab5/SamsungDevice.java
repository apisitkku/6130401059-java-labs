/**

 * 
 * SamsungDevice is to model a samsung device object with
 * attributes brand and androidVesion. 
 * 
 * Author: Apisit Aphiphatthanaphokhin
 * ID: 613040105-9
 * Sec: 1
 * Date: February 18, 2019
 *
 */
package aphiphatthanaphokhin.apisit.lab5;

public class SamsungDevice extends MobileDevice {

	private static String brand;
	private double androidVersion;

	public static String getBrand() {
		return brand;
	}

	public void setBrand(String brand) {
		SamsungDevice.brand = brand;
	}

	public double getAndroidVersion() {
		return androidVersion;
	}

	public void setAndroidVersion(double androidVersion) {
		this.androidVersion = androidVersion;
	}

	public SamsungDevice(String modelName, int price, double androidVersion) {
		super(modelName, "Android", price);
		this.androidVersion = androidVersion;
		brand = "Samsung";
		// TODO Auto-generated constructor stub
	}

	public SamsungDevice(String modelName, int price, int weight, double androidVersion) {
		super(modelName, "Android", price, weight);
		this.androidVersion = androidVersion;
		brand = "Samsung";
		// TODO Auto-generated constructor stub
	}

	@Override
	public String toString() {
		return "SamsungDevice [Model name :" + getModelName() + ", OS :" + getOs() + ", Price :" + getPrice()
				+ ", Weight :" + getWeight() + " g" + ", AndroidVersion :" + androidVersion + "]";
	}


	public void displayTime() {
		System.out.println("Display times in both using a digital format and using an analog watch");
		
	}  
	
	
}
